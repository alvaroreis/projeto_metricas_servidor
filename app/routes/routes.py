from app import app
from app.views import users, servidores, metricas, helper
from flask import jsonify


@app.route('/', methods=['GET'])
@helper.token_required
def root(current_users):
    return jsonify({'msg': f'Hello {current_users.nome}'})

"""
AUTENTICAÇÃO
"""
@app.route('/auth', methods=['POST'])
def authenticate():
    return helper.auth()

"""
USUARIOS
"""

@app.route('/usuarios', methods=['POST'])
def post_user():
    return users.post_user()


@app.route('/usuarios', methods=['GET'])
def get_users():
    return users.get_users()


@app.route('/usuarios/<codigo>', methods=['GET'])
def get_user_by_codigo(codigo):
    return users.get_user_by_codigo(codigo)


@app.route('/usuarios/<codigo>', methods=['PUT'])
def update_user(codigo):
    return users.update_user(codigo)


@app.route('/usuarios/<codigo>', methods=['DELETE'])
def delete_user(codigo):
    return users.delete_user(codigo)


"""
SERVIDORES
"""

@app.route('/servidores', methods=['POST'])
def post_servidor():
    return servidores.post_servidor()


@app.route('/servidores', methods=['GET'])
def get_servidores():
    return  servidores.get_servidores()


@app.route('/servidores/<codigo>', methods=['GET'])
def get_servidor_by_codigo(codigo):
    return servidores.get_servidor_by_codigo(codigo)


@app.route('/servidores/<codigo>', methods=['PUT'])
def update_servidor(codigo):
    return servidores.update_servidor(codigo)


@app.route('/servidores/<codigo>', methods=['DELETE'])
def delete_servidor(codigo):
    return servidores.delete_servidor(codigo)

"""
METRICAS
"""

@app.route('/metricas', methods=['POST'])
def post_metrica():
    return metricas.post_metrica()


@app.route('/metricas', methods=['GET'])
def get_metricas():
    return  metricas.get_metricas()


@app.route('/metricas/<codigo>', methods=['GET'])
def get_metrica_by_codigo(codigo):
    return metricas.get_metrica_by_codigo(codigo)


@app.route('/metricas/<codigo>', methods=['PUT'])
def update_metrica(codigo):
    return metricas.update_metrica(codigo)


@app.route('/metricas/<codigo>', methods=['DELETE'])
def delete_metrica(codigo):
    return metricas.delete_metrica(codigo)