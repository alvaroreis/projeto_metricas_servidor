from flask import request, jsonify


def mensagem(messagem, data=None):
    return jsonify({'message': messagem, 'data': data})