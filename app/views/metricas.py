from app import db
from flask import request
from app.utis.mensagem import mensagem
from app.views.servidores import get_servidor_by_dns
from app.models.metricas import Metricas, metrica_schema, metricas_schema


def get_metricas():
    metricas = Metricas.query.all()
    if metricas:
        resultado = metricas_schema.dump(metricas)
        return mensagem(messagem='Busca com sucesso.', data=resultado)

    return mensagem(messagem='Não existem métricas cadastradas.', data={})


def get_metrica_by_codigo(codigo):
    metrica = Metricas.query.get(codigo)
    if metrica:
        resultado = metrica_schema.dump(metrica)
        return mensagem(messagem='Busca realizada com sucesso.', data=resultado), 200

    return mensagem(messagem='Métrica não encontrada.', data={}), 404


def post_metrica():
    memoria_total = request.json['memoria_total']
    memoria_livre = request.json['memoria_livre']
    disco_livre = request.json['disco_livre']
    disco_total = request.json['disco_total']
    cpu_uso = request.json['cpu_uso']

    servidor_dns = request.json['servidor_dns']
    servidor = get_servidor_by_dns(servidor_dns)

    metrica = Metricas(memoria_total=memoria_total, memoria_livre=memoria_livre, disco_livre=disco_livre,
                       disco_total=disco_total, cpu_uso=cpu_uso, servidor=servidor)

    try:
        db.session.add(metrica)
        db.session.commit()
        resultado = metrica_schema.dump(metrica)
        return mensagem(messagem='Métrica registrada com sucesso.', data=resultado), 201
    except:
        return mensagem(messagem='Falha ao registrar métrica.', data={}), 500


def update_metrica(codigo):
    metrica = Metricas.query.get(codigo)

    if not metrica:
        return mensagem(messagem='Métrica não encontrada.', data={}), 404

    if 'memoria_total' in request.json:
        metrica.memoria_total = request.json['memoria_total']

    if 'memoria_total' in request.json:
        metrica.memoria_livre = request.json['memoria_livre']

    if 'disco_livre' in request.json:
        metrica.versao_sistema = request.json['disco_livre']

    if 'disco_total' in request.json:
        metrica.ip = request.json['disco_total']

    if 'cpu_uso' in request.json:
        metrica.cpu_uso = request.json['cpu_uso']

    if 'servidor_dns' in request.json:
        servidor_dns = request.json['servidor_dns']
        servidor = get_servidor_by_dns(servidor_dns)
        metrica.servidor_codigo = servidor.codigo

    try:
        db.session.commit()
        resultado = metrica_schema.dump(metrica)
        return mensagem(messagem='Métrica atualizada com sucesso.', data=resultado), 201
    except:
        return mensagem(messagem='Falha ao atualizar métrica.', data={}), 500


def delete_metrica(codigo):
    metrica = None
    try:
        metrica = Metricas.query.get(codigo)
    except:
        return mensagem(messagem='Métrica não encontrado.', data={}), 404

    if metrica:
        try:
            db.session.delete(metrica)
            db.session.commit()
            resultado = metrica_schema.dump(metrica)
            return mensagem(messagem='Métrica deletada com sucesso.', data={resultado}), 201
        except:
            return mensagem(messagem='Falha ao deletar métrica.', data={}), 500
