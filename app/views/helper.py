import jwt
import datetime
from app import app
from app.views.users import user_by_username
from werkzeug.security import check_password_hash
from flask import request, jsonify
from functools import wraps

SECRET_KEY = app.config['SECRET_KEY']
CABECALHO_AUTENTICACAO = 'WWW-Authenticate'


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = request.args.get('token')
        if not token:
            return jsonify({'message': 'Token não encontrado.', 'data': {}}), 401
        try:
            data = jwt.decode(token, SECRET_KEY)
            current_user = user_by_username(username=data['username'])
        except:
            return jsonify({'message': 'Token é inválido ou expirou.', 'data': {}}), 401.

        return f(current_user, *args, **kwargs)

    return decorated


def auth():
    auth = request.authorization
    if not auth or not auth.username or not auth.password:
        return jsonify(
            {'message': 'Não foi possível verificar.', CABECALHO_AUTENTICACAO: 'Basic auth="Login requerido"'}), 401

    user = user_by_username(auth.username)
    if not user:
        return jsonify({'message': 'Usuário não encontrado.', 'data': {}}), 401

    if user and check_password_hash(user.password, auth.password):
        token = jwt.encode({
            'username': user.username,
            'exp': datetime.datetime.now() + datetime.timedelta(hours=12)
        },
            SECRET_KEY)

        return jsonify({'message': 'Sucesso na vaidação.',
                        'token': token.decode('UTF-8'),
                        'exp': datetime.datetime.now() + datetime.timedelta(hours=12)
                        })
    return jsonify(
        {'message': 'Não foi possível verificar.', CABECALHO_AUTENTICACAO: 'Basic auth="Login requerido"'}), 401
