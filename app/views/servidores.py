from app import db
from flask import request
from app.utis.mensagem import mensagem
from app.models.servidores import Servidores, servidor_schema, servidores_schema


def get_servidores():
    servidores = Servidores.query.all()
    if servidores:
        resultado = servidores_schema.dump(servidores)
        return mensagem(messagem='Busca com sucesso.', data=resultado)

    return mensagem(messagem='Não existem servidores cadastrados.', data={})


def get_servidor_by_codigo(codigo):
    servidor = Servidores.query.get(codigo)
    if servidor:
        resultado = servidor_schema.dump(servidor)
        return mensagem(messagem='Busca realizada com sucesso.', data=resultado), 200

    return mensagem(messagem='Servidor não encontrado.', data={}), 404


def get_servidor_by_dns(dns):
    servidor = Servidores.query.filter_by(dns=dns).one  ()
    if servidor:
        return servidor

    return None


def post_servidor():
    hostname = request.json['hostname']
    sistema = request.json['sistema']
    versao_sistema = request.json['versao_sistema']
    ip = request.json['ip']
    dns = request.json['dns']
    servidor = Servidores(hostname, sistema, versao_sistema, ip, dns)

    try:
        db.session.add(servidor)
        db.session.commit()
        resultado = servidor_schema.dump(servidor)
        return mensagem(messagem='Servidor registrado com sucesso.', data=resultado), 201
    except:
        return mensagem(messagem='Falha ao registrar servidor.', data={}), 500


def update_servidor(codigo):
    servidor = Servidores.query.get(codigo)

    if not servidor:
        return mensagem(messagem='Servidor não encontrado.', data={}), 404

    if 'hostname' in request.json:
        servidor.hostname = request.json['hostname']

    if 'sistema' in request.json:
        servidor.sistema = request.json['sistema']

    if 'versao_sistema' in request.json:
        servidor.versao_sistema = request.json['versao_sistema']

    if 'ip' in request.json:
        servidor.ip = request.json['ip']

    if 'dns' in request.json:
        servidor.dns = request.json['dns']

    try:
        db.session.commit()
        resultado = servidor_schema.dump(servidor)
        return mensagem(messagem='Servidor atualizado com sucesso.', data=resultado), 201
    except:
        return mensagem(messagem='Falha ao atualizar servidor.', data={}), 500

def delete_servidor(codigo):
    servidor = None
    try:
        servidor = Servidores.query.get(codigo)
    except:
        return mensagem(messagem='Servidor não encontrado.', data={}), 404

    if servidor:
        try:
            db.session.delete(servidor)
            db.session.commit()
            resultado = servidor_schema.dump(servidor)
            return mensagem(messagem='Servidor deletado com sucesso.', data={resultado}), 201
        except:
            return mensagem(messagem='Falha ao deletar servidor.', data={}), 500