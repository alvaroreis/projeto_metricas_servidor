from werkzeug.security import generate_password_hash
from app import db
from flask import request
from app.utis.mensagem import mensagem
from app.models.users import Users, user_schema, users_schema


def user_by_username(username):
    try:
        return Users.query.filter(Users.username == username).one()
    except:
        return None


def get_users():
    users = Users.query.all()
    if users:
        resultado = users_schema.dump(users)
        return mensagem(messagem='Busca com sucesso.', data=resultado)

    return mensagem(messagem='Não existem usuários cadastrados.', data={})


def get_user_by_codigo(codigo):
    user = Users.query.get(codigo)
    if user:
        resultado = user_schema.dump(user)
        return mensagem(messagem='Busca realizada com sucesso.', data=resultado), 200

    return mensagem(messagem='Usuário não encontrado.', data={}), 404


def post_user():
    username = request.json['username']
    password = request.json['password']
    nome = request.json['nome']
    email = request.json['email']
    pass_hash = generate_password_hash(password)
    user = Users(username, pass_hash, nome, email)
    try:
        db.session.add(user)
        db.session.commit()
        resultado = user_schema.dump(user)
        return mensagem(messagem='Usuário registrado com sucesso.', data=resultado), 201
    except:
        return mensagem(messagem='Falha ao registrar usuário.', data={}), 500


def update_user(codigo):
    user = Users.query.get(codigo)
    if not user:
        return mensagem(messagem='Usuário não existe.', data={}), 404

    if 'username' in request.json:
        user.username = request.json['username']

    if 'password' in request.json:
        password = request.json['password']
        pass_hash = generate_password_hash(password)
        user.password = pass_hash

    if 'nome' in request.json:
        user.nome = request.json['nome']

    if 'email' in request.json:
        user.email = request.json['email']

    try:
        db.session.commit()
        resultado = user_schema.dump(user)
        return mensagem(messagem='Usuário atualizado com sucesso.', data=resultado), 201
    except:
        return mensagem(messagem='Falha ao atualizar usuário.', data={}), 500


def delete_user(codigo):
    user = None
    try:
        user = Users.query.get(codigo)
    except:
        return mensagem(messagem='Usuário não existe.', data={}), 404

    if user:
        try:
            db.session.delete(user)
            db.session.commit()
            resultado = user_schema.dump(user)
            return mensagem(messagem='Usuário deletado com sucesso.', data={resultado}), 201
        except:
            return mensagem(messagem='Falha ao deletar usuário.', data={}), 500
