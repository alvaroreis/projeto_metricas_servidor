import datetime
from app import db, ma
from uuid import uuid4
from sqlalchemy_utils import UUIDType
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import Column, DateTime, Integer, ForeignKey

'''Definição da classe/tabela dos servidores e seus campos'''


class Metricas(db.Model):
    __tablename__ = 'metrica'
    codigo = Column(UUID(as_uuid=True), default=uuid4, primary_key=True)
    memoria_total = Column(Integer)
    memoria_livre = Column(Integer)
    disco_livre = Column(Integer)
    disco_total = Column(Integer)
    cpu_uso = Column(Integer)
    create_on = Column(DateTime, default=datetime.datetime.now())
    servidor_codigo = Column(UUIDType, ForeignKey('servidor.codigo'))

    # def __init__(self, memoria_total, memoria_livre, disco_livre, disco_total,
    #              cpu_uso, servidor_codigo):
    #     self.memoria_total = memoria_total
    #     self.memoria_livre = memoria_livre
    #     self.disco_livre = disco_livre
    #     self.disco_total = disco_total
    #     self.cpu_uso = cpu_uso
    #     self.servidor_codigo = servidor_codigo
    #

    def porcentagem_dados(self=None, espaco_livre=None, espaco_total=None):
        resultado = round((espaco_livre / espaco_total) * 100, 2)
        return resultado


'''Definindo o Schema do Marshmallow para facilitar a utilização de JSON'''


class MetricasSchema(ma.Schema):
    class Meta:
        fields = ('codigo', 'memoria_total', 'memoria_livre', 'disco_total', 'disco_livre', 'cpu_uso', 'create_on',
                  'servidor_codigo')


metrica_schema = MetricasSchema()
metricas_schema = MetricasSchema(many=True)
