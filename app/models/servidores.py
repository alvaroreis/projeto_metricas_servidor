import datetime
from sqlalchemy.orm import relationship
from sqlalchemy import Column, String, DateTime
from sqlalchemy.dialects.postgresql import UUID
from app.models.metricas import MetricasSchema
from uuid import uuid4
from app import db, ma


'''Definição da classe/tabela dos servidores e seus campos'''
class Servidores(db.Model):
    __tablename__ = 'servidor'
    codigo = Column(UUID(as_uuid=True), default=uuid4, primary_key=True)
    hostname = Column(String(40), index=True)
    sistema = Column(String(40), index=True)
    versao_sistema = Column(String(100))
    ip = Column(String(11))
    dns = Column(String(150), index=True)
    create_on = Column(DateTime, default=datetime.datetime.now())
    metricas = relationship('Metricas', backref="servidor")

    def __init__(self, hostname, sistema, versao_sistema, ip, dns):
        self.hostname = hostname
        self.sistema = sistema
        self.versao_sistema = versao_sistema
        self.ip = ip
        self.dns = dns

'''Definindo o Schema do Marshmallow para facilitar a utilização de JSON'''
class ServidoresSchema(ma.Schema):
    # Lista de objetos de metrica
    metricas = ma.Nested(MetricasSchema, many=True)

    class Meta:
        fields = ('codigo', 'hostname', 'sistema', 'versao_sistema', 'ip', 'dns', 'create_on', 'metricas')


servidor_schema = ServidoresSchema()
servidores_schema = ServidoresSchema(many=True)