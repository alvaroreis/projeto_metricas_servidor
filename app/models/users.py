import datetime
from sqlalchemy import Column, String, DateTime
from sqlalchemy.dialects.postgresql import UUID

from uuid import uuid4
from app import db, ma


'''Definição da classe/tabela dos usuários e seus campos'''
class Users(db.Model):
    __tablename__ = 'user'
    codigo = Column(UUID(as_uuid=True), default=uuid4, primary_key=True)
    username = Column(String(20), unique=True, nullable=False)
    password = Column(String(200), nullable=False)
    nome = Column(String(60), nullable=False)
    email = Column(String(50), unique=True, nullable=False)
    create_on = Column(DateTime, default=datetime.datetime.now())

    def __init__(self, username, password, nome, email):
        self.username = username
        self.password = password
        self.nome = nome
        self.email = email


'''Definindo o Schema do Marshmallow para facilitar a utilização de JSON'''
class UsersSchema(ma.Schema):
    class Meta:
        fields = ('codigo', 'username', 'nome', 'email', 'password', 'create_on')


user_schema = UsersSchema()
users_schema = UsersSchema(many=True)
