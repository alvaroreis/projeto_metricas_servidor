from flask import Flask
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

'''
CONFIGURAÇÃO DO FLASK
'''
app = Flask(__name__)
app.config.from_object('app.config')
# api = Api(app)

'''
CONFIGURAÇÃO DO BANCO DE DADOS
'''
db = SQLAlchemy(app)
ma = Marshmallow(app)

'''
CONFIGURAÇÃO DOS MODELOS, ROTAS
'''
from app.models import users, metricas, servidores
from app.routes import routes
