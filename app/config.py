from app import app
import string
import random


random_str = string.ascii_letters + string.digits + string.ascii_uppercase
key = ''.join(random.choice(random_str) for i in range(12))

app.debug=True
DEBUG = True
SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://postgres:root@172.17.0.3/iec_metricas"
SQLALCHEMY_TRACK_MODIFICATIONS = False
SECRET_KEY = key